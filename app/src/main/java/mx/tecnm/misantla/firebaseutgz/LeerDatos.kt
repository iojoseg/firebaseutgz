package mx.tecnm.misantla.firebaseutgz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.firebaseutgz.databinding.ActivityLeerDatosBinding

class LeerDatos : AppCompatActivity() {

    private lateinit var binding : ActivityLeerDatosBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeerDatosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnBuscar.setOnClickListener {

            val nombre : String = binding.edtDatoNombre.text.toString()

            if(nombre.isNotEmpty()){
                leerDatos(nombre)
            }else{
                Toast.makeText(this,"Ingrese un usuario",Toast.LENGTH_LONG).show()
            }
        }


    }

    private fun leerDatos(nombre: String) {
         database = FirebaseDatabase.getInstance().getReference("basededatos")
        database.child(nombre).get().addOnSuccessListener {
            if(it.exists()){
                val nombre = it.child("nombre").value
                val apellido = it.child("apellidos").value
                val edad = it.child("edad").value

                binding.edtDatoNombre.text.clear()

                binding.tvNombre.text = nombre.toString()
                binding.tvApellido.text = apellido.toString()
                binding.tvEdad.text = edad.toString()

            }else{
                Toast.makeText(this,"Usuario no existe",Toast.LENGTH_LONG).show()
            }
        }.addOnFailureListener {
            Toast.makeText(this,"Fallo...Error",Toast.LENGTH_LONG).show()
        }

    }
}