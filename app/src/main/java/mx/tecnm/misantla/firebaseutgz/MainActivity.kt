package mx.tecnm.misantla.firebaseutgz

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.firebaseutgz.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private  lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

     binding.btnRegister.setOnClickListener {

         val nombre = binding.edtNombre.text.toString()
         val apellidos = binding.edtApellido.text.toString()
         val edad = binding.edtEdad.text.toString()

         database = FirebaseDatabase.getInstance().getReference("basededatos")
         val datos = User(nombre, apellidos, edad)
         database.child(nombre).setValue(datos)

         Toast.makeText(this,"Informacion guardada",Toast.LENGTH_LONG).show()

         binding.edtNombre.text.clear()
         binding.edtApellido.text.clear()
         binding.edtEdad.text.clear()
     }

         binding.btnLectura.setOnClickListener {
             val intent = Intent(this,LeerDatos::class.java)
             startActivity(intent)
         }

        binding.btnDelete.setOnClickListener {
            val intent = Intent(this,DeleteDatos::class.java)
            startActivity(intent)
        }

        binding.btnActualizar.setOnClickListener {
            val intent = Intent(this,ActualizarDatos::class.java)
            startActivity(intent)
        }

    }
}