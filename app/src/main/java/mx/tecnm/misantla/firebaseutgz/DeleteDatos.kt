package mx.tecnm.misantla.firebaseutgz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.firebaseutgz.databinding.ActivityDeleteDatosBinding

class DeleteDatos : AppCompatActivity() {

    private lateinit var binding: ActivityDeleteDatosBinding
    private  var reference : DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeleteDatosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEliminar.setOnClickListener {

            var usuario : String = binding.edtNombreD.text.toString()

            if(!usuario.isEmpty()){
                borrarUsuario(usuario)
            }else{
                Toast.makeText(this,"Ingrese un usuario", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun borrarUsuario(usuario: String) {
        reference = FirebaseDatabase.getInstance().getReference("basededatos")
        reference!!.child(usuario).removeValue().addOnCompleteListener {
         if(it.isSuccessful){
             Toast.makeText(this,"Se borro  correctamente", Toast.LENGTH_LONG).show()
             binding.edtNombreD.text.clear()
         }  else{
             Toast.makeText(this,"No se pudo borrar", Toast.LENGTH_LONG).show()
         }
        }

    }
}