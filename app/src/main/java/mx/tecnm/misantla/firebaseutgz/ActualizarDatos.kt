package mx.tecnm.misantla.firebaseutgz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.firebaseutgz.databinding.ActivityActualizarDatosBinding

class ActualizarDatos : AppCompatActivity() {

    private lateinit var binding : ActivityActualizarDatosBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityActualizarDatosBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnActualizar.setOnClickListener {
            val nombre = binding.edtAnombre.text.toString()
            val apellido = binding.edtAapellido.text.toString()
            val edad = binding.edtAedad.text.toString()

            updateDatos(nombre,apellido,edad)
        }
    }

    private fun updateDatos(nombre: String, apellido: String, edad: String) {

        database = FirebaseDatabase.getInstance().getReference("basededatos")

        val user = mapOf<String,String>(
            "nombre" to nombre,
            "apellidos" to apellido,
            "edad" to edad
        )

        database.child(nombre).updateChildren(user).addOnCompleteListener {
            binding.edtAnombre.text.clear()
            binding.edtAapellido.text.clear()
            binding.edtAedad.text.clear()

            Toast.makeText(this, "Se actualizo la informacion",Toast.LENGTH_LONG).show()
        }.addOnFailureListener {
            Toast.makeText(this, "Error de Actualizacion!!!",Toast.LENGTH_LONG).show()
        }
    }
}